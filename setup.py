"""A setuptools based setup module.
See:
https://python-packaging.readthedocs.io/en/latest/minimal.html
https://packaging.python.org/guides/distributing-packages-using-setuptools/
https://github.com/pypa/sampleproject
"""

# Always prefer setuptools over distutils
from setuptools import setup, find_packages
from os import path
# io.open is needed for projects that support Python 2.7
# It ensures open() defaults to text mode with universal newlines,
# and accepts an argument to specify the text encoding
# Python 3 only projects can skip this import
from io import open

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

# Arguments marked as "Required" below must be included for upload to PyPI.
# Fields marked as "Optional" may be commented out.

setup(
    name='edfreadpy',  # Required
    version='0.1',  # Required

    description='A EDF(+) reader for Python',  # Optional
    long_description=long_description,  # Optional
    long_description_content_type='text/markdown',  # Optional (see note above)

    author='Tim Hermans',  # Optional
    author_email='tim.hermans@esat.kuleuven.be',  # Optional

    keywords='edf reader',  # Optional

    packages=find_packages(exclude=['contrib', 'docs', 'tests', 'examples', 'venv']),  # Required

    python_requires='>=3.0',

    install_requires=[  # Optional
        'numpy',
        'pandas',
    ],

    project_urls={  # Optional
        'Source': 'https://gitlab.com/timhermans/edfreadpy',
    },
)