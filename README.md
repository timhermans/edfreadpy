<!-- ABOUT THE PROJECT -->
## About The Project
Reader for EDF and EDF+ files in Python.
Includes functionality to read annotations and to anonymize the EDF header.

This EDF reader is now included in the NeoNatal Signal Analysis ([nnsa](https://gitlab.com/timhermans/nnsa_public)) package. 

<!-- GETTING STARTED -->
## Getting Started

### Installation
To install this package:
1. cd to the path where you want to store the source code, e.g.:
```sh
cd C:/git_projects
```

2. Clone the repo:
```sh
git clone https://gitlab.com/timhermans/edfreadpy.git
```

3. Enter/activate the Python environment in which you want to install the package, e.g. using conda:
```sh
conda activate <your_env_name>
```

4. Install the package, using the path to the local copy of the source code as determined in step 1, e.g.:
```sh
pip install C:/git_projects/edfreadpy
```

Or, if you want to be able to edit the source code, without needing to reinstall the package, use the editable flag:
```sh
pip install -e C:/git_projects/edfreadpy
```

<!-- USAGE EXAMPLES -->
## Usage
```python
from edfreadpy import EdfReader

# Specify filepath to an EDF(+) file.
filepath = 'your_edf_file.EDF'

# Use a context manager, which automatically closes the file when leaving the context block.
with EdfReader(filepath) as r:
    # Read first signal in the file (first channel).
    signal = r.read_signal(channel=0)
    signal_label = r.signal_headers['label'][0]

print('Done reading signal "{}": {}'.format(signal_label, signal))
```

See the tutorial script [examples/edf_reader.py](examples/edf_reader.py) for more examples.

<!-- CONTACT -->
## Contact

Tim Hermans - tim.hermans@esat.kuleuven.be

Project Link: [https://gitlab.com/timhermans/edfreadpy](https://gitlab.com/timhermans/edfreadpy)

