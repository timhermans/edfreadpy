"""
This script demonstrates how the edfreadpy.EdfReader can be used to read EDF(+) files.
"""
import os
from edfreadpy import EdfReader
from edfreadpy.io.config import EDFPLUS_TYPES

# Specify path to an EDF(+) file.
filepath = 'C:\data_temp\test.edf'

# The EdfReader can be used as a context manager, to ensure the EDF file is closed after reading.
with EdfReader(filepath) as r:

    # The EdfReader has properties, that can be accessed like attributes. Those properties will only hold data once they
    # are called. I.e. when initializing an EdfReader object, nothing is read from the file into memory. Once when
    # e.g. EdfReader.file_header is called, the file header is read from the EDF file and stored in the EdfReader
    # object. The file header stored in the EdfReader object can be accessed later via EdfReader.file_header (without
    # having to read the EDF header again). Through the use of a property, the syntax for getting the file header is the
    # same, whether the file header was already read into the memory or not.

    # Read/get the file header.
    hdr = r.file_header

    # r.file_header returns a dictionary, in which the keys are strings representing the fields in the (normal) EDF
    # header (no EDF+ fields are returned by r.file_header). The values are the entries in the corresponding fields
    # stored as a suitable data type (e.g. str, int, float).
    print('\nFile header:', hdr)

    # E.g. print the number of signals in a the file (last field in the EDF header):
    print('Number of signals in file:', hdr['num_signals'])

    # Print all keys of the header dictionary:
    print('Keys in r.file_header:', list(hdr.keys()))

    # Read/get the signal headers.
    s_hdrs = r.signal_headers

    # r.signal_headers returns a dictionary, in which the keys are strings representing the fields in an EDF signal
    # header. The values are lists that contain the entries for the corresponding fields for each signal in the file,
    # stored as a suitable data type (e.g. str, int, float).
    print('\nSignal headers:', s_hdrs)

    # E.g. print the list of labels of all the signals in the file:
    print('Signal labels:', s_hdrs['label'])

    # E.g. get the physical dimension of the 3rd signal (index 2) in the file.
    print('Physical dimension of 3rd signal:', s_hdrs['physical_dimension'][2])

    # Print the list of all keys in the dictionary:
    print('Keys in r.signal_headers:', list(s_hdrs.keys()))

    # Read/get additional information about the file and the signals. This will also include the EDF+ fields in case of
    # an EDF+ file.
    a_info = r.additional_info

    # r.additional_info returns a dictionary, in which the keys are strings representing some kind of information about
    # the file or signals in the file.
    print('\nAdditional info:', a_info)

    # E.g. print the file type (EDF or EDF+):
    print('File type:', a_info['filetype'])

    # Or, in case of EDF+ file, print the investigator:
    if a_info['filetype'] in EDFPLUS_TYPES:
        print('Investigator:', a_info['recording_id']['investigator'])

    # Print the list of all keys in the dictionary:
    print('Keys in r.additional_info:', list(a_info.keys()))

    # Using the property is_anonymized, you can check whether the loaded header information is anonymized (see the
    # is_anonymized code in EdfReader to see which check is performed).
    print('Is anonymized:', r.is_anonymized)

    # We can anonymize the header information in place:
    r.anonymize(seed_offset=3)
    print('Is anonymized:', r.is_anonymized)

    # We can then create a new EDF(+) file with the anonymized header information:
    base, filename = os.path.split(filepath)
    filepath_out = os.path.join(base, 'anonymized', filename)
    r.anonymize_and_save(filepath_out, seed_offset=3)

# Let us now open the anonymized file and read some signals.
with EdfReader(filepath_out) as r:

    # Verify that it is anonymized:
    print('Anonymized file is anonymized:', r.is_anonymized)

    # To read data of a particular channel, use r.read_signal() and specify the channel either by its index or label.
    # E.g. read the entire first signal (first channel) in the EDF file. Note that the channel count starts at
    # zero like indexing in Python).
    channel_index = 0
    signal_0 = r.read_signal(channel=channel_index)

    # Read the entire second channel, by specifying its label.
    channel_label = r.signal_headers['label'][1]
    signal_1 = r.read_signal(channel=channel_label)

    # To only read a specific part of a signal, specify the index of the start and the stop sample.
    # E.g. read first 500 samples of the signal in the third channel.
    signal_2 = r.read_signal(channel=2, start=0, stop=500, efficiency='memory')

    # Note that the read_signal() method accepts another optional argument, efficiency. You can set it either to
    # 'memory' or 'speed'. The difference between the two options is that for the 'memory' option, only the bytes of the
    # signal you requested are read from the file. Therefore, the amount of memory it uses is proportional to the amount
    # of data that you want to read from the file. On the other hand, with the 'speed' option, the entire file is
    # read into the memory and only after reading, the parts you requested are selected and returned. This latter
    # approach is typically faster (unless you only want to read a small part of a signal). Therefore, the efficiency
    # argument defaults to 'speed'. I suggest to use the 'memory' option only when you have problems with the entire
    # EDF file fitting in the RAM.

    # If you have used efficiency='speed', the entire (raw) data will be read in the memory and stored in the EdfReader
    # object for quick (later) access (to prevent having to read the entire file again if you want to read another
    # channel). You can flush this data, freeing some memory (approx. the size of the EDF file):
    r.flush_all_digital_data()

    # In case of an EDF+ file, you can read annotations in the file:
    if a_info['filetype'] in EDFPLUS_TYPES:
        annotations = r.read_annotations()

        # r.read_annotations() will collect each annotation as an edfreadpy.Annotation object in a
        # edfreadpy.AnnotationSet object.
        # The Annotation object is a simple object having three attributes: text, onset, duration.
        # The AnnotationSet is basically just a list of Annotation objects.
        # Check out the class definitions in the annotations directory for more information.
        print(annotations)
        annotations.print_all_annotations()

# Remove the anonymized file that was created.
os.remove(filepath_out)


