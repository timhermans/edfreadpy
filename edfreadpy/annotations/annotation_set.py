"""
Code related to annotation sets.
"""
import copy
import numpy as np
import pandas as pd

from edfreadpy.annotations.annotation import Annotation

__all__ = [
    'AnnotationSet',
]


class AnnotationSet(object):
    """
    Holds a collection of annotations (e.g. all annotations from one file/investigator).

    Args:
        annotations (iterable, optional): iterable collection with Annotation objects (e.g. list, generator).
        label (str, optional): a label for this AnnotationSet (e.g. code/name of the observer).
    """
    def __init__(self, annotations=None, label='annotations'):
        self.label = label
        self._annotations = []

        # Append annotations given in annotations_list to the annotation set.
        if annotations is not None:
            self.extend(annotations, inplace=True)

    def __getitem__(self, item):
        """
        Return annotation at index `item`.
        """
        return self.annotations[item]

    def __iter__(self):
        """
        Return iterator that iterates over Annotation objects in this AnnotationSet.

        This makes it possible to iterate over the annotations list by iterating over the AnnotationSet directly
        instead of having to iterate over AnnotationSet.annotations.

        Returns:
            (list_iterator): iterator that iterates over Annotation objects in this AnnotationSet.
        """
        # Return the iterator of the list that holds the Annotation objects.
        return self._annotations.__iter__()

    def __len__(self):
        """
        Return the number of the annotations in the set.

        Returns:
            (int): length of annotations list.
        """
        return len(self.annotations)

    def __repr__(self):
        """
        Return a comprehensive info string about this object.

        Returns:
            (str): a comprehensive info string about this object.
        """
        with pd.option_context('display.max_rows', None, 'display.max_columns', None):
            return '{} with label: "{}" and annotations:\n{}'.format(self.__class__.__name__, self.label,
                                                                     self.to_dataframe())

    @property
    def annotations(self):
        """
        Return the list with annotations.

        Returns:
            (list): list with edfreadpy.Annotation objects.
        """
        return self._annotations

    def add_postfix(self, postfix, inplace=False):
        """
        Add postfix to all annotations.

        Args:
            prefix (str): string to add after each Annotation's text.
            inplace (bool, optional): if True, modify the annotation in place.
                If False, make a copy and modify the copy.

        Returns:
            annotation_set (edfreadpy.AnnotationSet): new AnnotationSet object with the modified annotations
                (if inplace is False).
        """
        if inplace:
            annotation_set = self
        else:
            # Create a copy of the annotation set (to preserve the original one).
            annotation_set = copy.deepcopy(self)

        # Iterate over the to be added annotations.
        for annot in annotation_set:
            annot.text = annot.text + postfix

        if not inplace:
            return annotation_set

    def add_prefix(self, prefix, inplace=False):
        """
        Add prefix to all annotations.

        Args:
            prefix (str): string to add in front of each Annotation's text.
            inplace (bool, optional): if True, modify the annotation in place.
                If False, make a copy and modify the copy.

        Returns:
            annotation_set (edfreadpy.AnnotationSet): new AnnotationSet object with the modified annotations
                (if inplace is False).
        """
        if inplace:
            annotation_set = self
        else:
            # Create a copy of the annotation set (to preserve the original one).
            annotation_set = copy.deepcopy(self)

        # Iterate over the to be added annotations.
        for annot in annotation_set:
            annot.text = prefix + annot.text

        if not inplace:
            return annotation_set

    def append(self, annotation, inplace=False):
        """
        Safely append an annotation to the annotation set.

        Args:
            annotation (edfreadpy.Annotation): Annotation object.
            inplace (bool, optional): if True, appends the annotation in place. If False, a new AnnotationSet object
                with the appended annotation is returned.
                Defaults to False.

        Returns:
            annotation_set (edfreadpy.AnnotationSet): new AnnotationSet object with the appended annotation
                (if inplace is False).
        """
        # Check whether to be appended object is an annotation object.
        if not isinstance(annotation, Annotation):
            raise ValueError('Invalid object of type "{}" to append to {}. Object should be "Annotation"'
                             .format(type(annotation).__name__, type(self).__name__))

        if inplace:
            # Append annotation to annotation list.
            self._annotations.append(annotation)
        else:
            # Create a copy of the annotation set (to preserve the original one).
            annotation_set = copy.deepcopy(self)
            annotation_set._annotations.append(annotation)
            return annotation_set

    def durations(self):
        """
        Return the durations of all annotations in the set.

        Returns:
            (np.ndarray): the durations (in seconds) of all annotations in the set.
        """
        return np.array([a.duration for a in self.annotations])

    def end_time(self):
        """
        Return the time at which the last annotation ends.

        Returns:
            (float): the time (in seconds) at which the last annotation ends.
        """
        last_annot = self.sort().annotations[-1]
        return last_annot.onset + last_annot.duration

    def extend(self, annotations, inplace=False):
        """
        Safely extend the list with annotation with another annotations from an iterable collection.

        Args:
            annotations (collection): iterable collection with Annotation objects (e.g. list, generator).
            inplace (bool, optional): if True, extends the annotations in place. If False, a new AnnotationSet object
                extended with the annotations is returned.
                Defaults to False.

        Returns:
            annotation_set (edfreadpy.AnnotationSet): new AnnotationSet object with the appended annotations
                (if inplace is False).
        """
        if inplace:
            annotation_set = self
        else:
            # Create a copy of the annotation set (to preserve the original one).
            annotation_set = copy.deepcopy(self)

        # Iterate over the to be added annotations and append them.
        for annot in annotations:
            annotation_set.append(annot, inplace=True)

        if not inplace:
            return annotation_set

    def extract_epoch(self, begin=None, end=None):
        """
        Extract epoch specified by begin and end.

        Args:
            begin (float): begin time of epoch (seconds).
            end (float): end time of epoch (seconds).

        Returns:
            (nnsa.AnnotationSet): subset of the current AnnotationSet.
        """
        if begin is None:
            begin = -np.inf
        if end is None:
            end = np.inf

        # Call get_overlapping_annotations with clip = True.
        return self.get_overlapping_annotations(begin=begin, end=end, clip=True)

    def filter(self, patterns, how='keep', case_sensitive=True, inplace=False):
        """
        Filter the annotation by looking for matching patterns in the texts.

        Args:
            patterns (str or list): pattern(s) to find matches for.
            how (str, optional): whether to keep or remove the annotations that match the patterns.
                Options:
                    - 'keep'
                    - 'remove'
                Defaults to 'keep'.
            case_sensitive (bool, optional): whether the pattern matching should be case sensitive.
                Defaults to True.
            inplace (bool, optional): if True, filters the annotations in place. If False, a new AnnotationSet
                object with the filtered annotations is returned.
                Defaults to False.

        Returns:
            as_out (edfreadpy.AnnotationSet): new AnnotationSet with a subset of the annotations.

        Examples:
            >>> annot_set = AnnotationSet()
            >>> annot_set.append(Annotation(onset=10.25, duration=100, text='QS'), inplace=True)
            >>> annot_set.append(Annotation(110.25, 200, 'AS'), inplace=True)
            >>> annot_set.append(Annotation(500, 300, 'QS'), inplace=True)
            >>> annot_set.append(Annotation(800, 10, 'AS'), inplace=True)
            >>> annot_set.print_all_annotations()
                onset  duration text
            0   10.25       100   QS
            1  110.25       200   AS
            2  500.00       300   QS
            3  800.00        10   AS
            >>> as_new = annot_set.filter('QS', how='keep')
            >>> as_new.print_all_annotations()
                onset  duration text
            0   10.25     100.0   QS
            1  500.00     300.0   QS
            >>> as_new = annot_set.filter('QS', how='remove')
            >>> as_new.print_all_annotations()
                onset  duration text
            0  110.25       200   AS
            1  800.00        10   AS
        """
        if not isinstance(patterns, list):
            patterns = [patterns]

        def fun_keep(text):
            # Function that returns True if any pattern in text, else False.
            keep = False
            for pat in patterns:
                if case_sensitive:
                    keep = pat in text
                else:
                    keep = pat.lower() in text.lower()
                if keep:
                    break  # Exit loop.
            return keep

        def fun_remove(text):
            # Function that returns True if None of the pattern in text, else False.
            keep = True
            for pat in patterns:
                if case_sensitive:
                    keep = pat not in text
                else:
                    keep = pat.lower() not in text.lower()
                if not keep:
                    break
            return keep

        if how.lower() == 'keep':
            return self.filter_fun(fun=fun_keep, pass_text=True, inplace=inplace)
        elif how.lower() == 'remove':
            return self.filter_fun(fun=fun_remove, pass_text=True, inplace=inplace)
        else:
            raise ValueError('Invalid option for `how`: "{}". Choose from: {}.'.format(
                how, ['keep', 'remove']))

    def filter_fun(self, fun, pass_text=True, inplace=False):
        """
         Filter the annotation keeping only annotations that yield True after evaluating fun on them.

         Args:
             fun (function): function that takes in a string or Annotation object (depending on `pass_text`)
                and returns True or False. Annotations that yield False are removed.
             pass_text (bool): if Ture, the annotation text (str) is passed to `fun`, otherwise the Annotation
                 object is passed.
             inplace (bool, optional): if True, filters the annotations in place. If False, a new AnnotationSet
                 object with the filtered annotations is returned.
                 Defaults to False.

         Returns:
             as_out (edfreadpy.AnnotationSet): new AnnotationSet with annotations that match the specified
                 pattern (only retruned if inplace is False).
         """
        if inplace:
            as_out = self
        else:
            # Create a copy of the annotation set (to preserve the original one).
            as_out = copy.deepcopy(self)

        # Create new set and append each annotation that matches the pattern.
        idx = 0
        while idx < len(as_out.annotations):
            an_i = as_out.annotations[idx]
            if pass_text:
                # Only use the text.
                an_i = an_i.text

            # Check if any pattern matches the text.
            keep = fun(an_i)

            # Check if we should keep the annotation.
            if not keep:
                as_out.annotations.pop(idx)
                # Do not increment idx, since we just popped one out of the list.
            else:
                # Go the next one by incrementing idx.
                idx += 1

        if not inplace:
            return as_out

    def from_df(self, df, onset='onset', duration='duration', text='text', **kwargs):
        """
        Convert a DataFrame to an AnnotationSet.

        Args:
            df (pd.DataFrame): a DataFrame.
            onset (str, optional): column name in df that corresponds to the onset times (in seconds).
            duration: (str, optional): column name in df that corresponds to the duration times (in seconds).
            text: (str, optional): column name in df that corresponds to the annotation text.
            **kwargs (optional): optional keyword arguments for AnnotationSet().

        Returns:
            annotation_set (AnnotationSet): AnnotationSet.
        """
        annotation_set = self.from_lists(df[onset], df[duration], df[text])

        return annotation_set

    def from_lists(self, onsets, durations, texts, **kwargs):
        """
        Convert a DataFrame to an AnnotationSet.

        Args:
            onsets (list, np.ndarray): list with the onset times (in seconds).
            durations (list, np.ndarray): list with the duration times (in seconds).
            texts (list, np.ndarray): list with the annotation texts.
            **kwargs (optional): optional keyword arguments for AnnotationSet().

        Returns:
            annotation_set (AnnotationSet): AnnotationSet.
        """
        # Initialize empty annotation set.
        annotation_set = self.__class__(**kwargs)

        # Go over entries in df.
        for (on, dur, t) in zip(onsets, durations, texts):
            annotation_set.append(Annotation(on, dur, t), inplace=True)

        return annotation_set

    def from_mask(self, mask, time=None, label_mapping=None, label=None):
        """
        Convert a mask with arbitrary number of classes to an annotation set.

        Args:
            mask (np.ndarray): 1D mask array containing discrete values.
            time (np.ndarray): time array corresponding to starttimes of `mask`.
                If None, the onset and durations in the returned AnnotationSet are
                in samples.
            label_mapping (dict, optional): dictionary mapping a value in the mask to a label (str).
                Those labels will become the texts in the annotation set.
                If None, the mask values will be the labels.
            label (str, optional): label for the annotation set.
                Defaults to None.

        Returns:
            annotation_set (AnnotationSet): annotation set with onsets, durations and texts.

        Examples:
            >>> time = np.arange(10)
            >>> mask = [0, 0, 1, 1, 1, 0, 0, 2, 2, 2]
            >>> AnnotationSet().from_mask(time, mask)
            AnnotationSet with label: "None" and annotations:
               onset  duration text
            0      0         2    0
            1      2         3    1
            2      5         2    0
            3      7         3    2
            >>> AnnotationSet().from_mask(time, mask, label_mapping={0: 'null', 1: 'one', 2: 'two'})
            AnnotationSet with label: "None" and annotations:
               onset  duration  text
            0      0         2  null
            1      2         3   one
            2      5         2  null
            3      7         3   two
        """
        # Check input.
        mask = np.asarray(mask).squeeze()
        if time is None:
            time = np.arange(len(mask))
        else:
            time = np.asarray(time).squeeze()
        if time.shape != mask.shape:
            raise ValueError('`time` and `mask` should have the same shape.')
        if time.ndim != 1:
            raise ValueError('`time` and `mask` should be 1-dimensional arrays.')

        if label_mapping is None:
            label_mapping = dict()

        # Initialize empty annotation set.
        annotation_set = self.__class__(label=label)

        # Extrapolate the time vector with one sample.
        time = np.append(time, 2*time[-1] - time[-2])

        # Add epochs with same mask value to annotation set.
        transition_idx = np.append(np.nonzero(np.diff(mask))[0], len(mask) - 1)
        t_onset = time[0]
        for idx in transition_idx:
            # Compute duration.
            duration = time[idx + 1] - t_onset

            # Extract text label.
            mask_value = mask[idx]
            label = label_mapping.get(mask_value, str(mask_value))

            # Add annotation.
            annotation = Annotation(onset=t_onset, duration=duration, text=label)
            annotation_set.append(annotation, inplace=True)

            t_onset = time[idx + 1]

        return annotation_set

    def get_overlapping_annotations(self, begin=-np.inf, end=np.inf, clip=False, start=None):
        """
        Return a sub set of the annotations with annotations that (partially) overlap with the specified time interval.

        Args:
            begin (float): begin time in seconds of the time interval.
            end (float): end time in seconds of the time interval.
            clip (bool): if True, clips the first and last annotations to the begin and end times.
                If False, keeps original lengths.
            start (flaot): overrides `begin` (for backwards compatibility).

        Returns:
            (nnsa.AnnotationSet): subset of the current AnnotationSet.
        """
        if start is not None:
            # For backwards compatibility.
            begin = start

        # Create an empty annotation object with same properies as current.
        annotation_set = copy.deepcopy(self)
        annotation_set._annotations = []

        # Loop over annotations (copy such that clipping is never inplace).
        for annot in copy.deepcopy(self):
            an_start = annot.onset
            an_end = an_start + annot.duration
            if begin <= an_start < end or begin <= an_end <= end or an_start <= begin <= an_end:
                if clip:
                    # Clip to begin and end times.
                    if annot.onset < begin:
                        dt = begin - annot.onset
                        annot.onset = begin
                        annot.duration -= dt
                    if (annot.onset + annot.duration) > end:
                        annot.duration = end - annot.onset
                annotation_set.append(annot, inplace=True)

        return annotation_set

    def remove(self, patterns, case_sensitive=True, inplace=False):
        """
        Filter the annotation by looking for matching patterns in the texts.

        Args:
            patterns (str or list): pattern(s) to find matches for.
            inplace (bool, optional): if True, removes the annotations in place. If False, a new AnnotationSet
                object without the removed annotations is returned.
                Defaults to False.

        Returns:
            as_out (edfreadpy.AnnotationSet): new AnnotationSet without annotations that match the specified
                pattern (only retruned if inplace is False).

        Examples:
            >>> annot_set = AnnotationSet()
            >>> annot_set.append(Annotation(onset=10.25, duration=100, text='QS'), inplace=True)
            >>> annot_set.append(Annotation(110.25, 200, 'AS'), inplace=True)
            >>> annot_set.append(Annotation(500, 300, 'QS'), inplace=True)
            >>> annot_set.append(Annotation(800, 10, 'AS'), inplace=True)
            >>> annot_set.print_all_annotations()
                onset  duration text
            0   10.25       100   QS
            1  110.25       200   AS
            2  500.00       300   QS
            3  800.00        10   AS
            >>> as_new = annot_set.remove('QS')
            >>> as_new.print_all_annotations()
                onset  duration text
            0  110.25       200   AS
            1  800.00        10   AS
        """
        return self.filter(patterns=patterns, how='remove', case_sensitive=case_sensitive, inplace=inplace)

    def onsets(self):
        """
        Return the onsets of all annotations in the set.

        Returns:
            (np.ndarray): the onsets (in seconds) of all annotations in the set.
        """
        return np.array([a.onset for a in self.annotations])

    def print_annotation(self, index, print_header=True):
        """
        Print the onset, text and duration of an annotation specified by the index.

        Args:
            index (int): the index of the anntation in self.annotations list to print.
            print_header (bool, optional): if True, a header will be printed, if False, not.
                Defaults to True.
        """
        # The onset, text and duration should be separated by fixed widths.
        string_format_header = '{:<12} {:<14} {:<60}'
        string_format_data = '{:<12.2f} {:<14.2f} {:<60}'

        if print_header:
            print(string_format_header.format('Onset (s)', 'Duration (s)', 'Text'))

        annot = self.annotations[index]
        line = string_format_data.format(annot.onset, annot.duration, annot.text)
        print(line)

    def print_all_annotations(self):
        """
        Print all annotations at once using a pandas dataframe.
        """
        # Make sure it prints all rows and columns.
        with pd.option_context('display.max_rows', None, 'display.max_columns', None):
            print(self.to_dataframe())

    def sort(self, inplace=False):
        """
        Sort the annotations based on onset.

        Args:
            inplace (bool, optional): if True, sorts the annotations in place. If False, a new AnnotationSet object with
                the sorted annotations is returned.
                Defaults to False.

        Returns:
            sorted_annotations (edfreadpy.AnnotationSet): new AnnotationSet object with the sorted annotations
                (if inplace is False).
        """
        # Function that extracts onset for sorting.
        def f(x): return x.onset

        if inplace:
            self.annotations.sort(key=f)
        else:
            # Create a copy of the annotations (to preserve the original ones).
            sorted_annotations = copy.deepcopy(self)

            # Sort by onset.
            sorted_annotations.annotations.sort(key=f)

            return sorted_annotations

    def start_time(self):
        """
        Return the onset time of the first annotation (in seconds).

        Returns:
            (float): onset time of first annotation (in seconds).
        """
        return self.sort().annotations[0].onset

    def subtract_offset(self, offset, inplace=False):
        """
        Subtract a constant amount of time from the onset of each annotation in the annotation set.

        May be used to perfectly align the annotations with the recorded signals, in case the recording of the signals
        did not start at the exact (whole) second defined as the starttime the fileheader. In EDF+ files, the EDF
        Annotation channel contains the (fractional) onset of each datarecord w.r.t. the starttime defined in the file
        header.

        Args:
            offset (float): the amount of time to subtract from the annotation onsets (in seconds).
            inplace (bool, optional): if True, subtracts the offset from the annotations in place. If False, a new
                AnnotationSet object with the edited annotations is returned.
                Defaults to False.

        Returns:
            annotation_set (edfreadpy.AnnotationSet): new AnnotationSet object with the edited annotations
                (if inplace is False).
        """
        if inplace:
            annotation_set = self
        else:
            # Create a copy of the annotation set (to preserve the original one).
            annotation_set = copy.deepcopy(self)

        # Change the onset of each Annotation object in place.
        for annot in annotation_set.annotations:
            annot.onset -= offset

        if not inplace:
            return annotation_set

    def texts(self):
        """
        Return the texts of all annotations in the set.

        Returns:
            (np.ndarray): the texts of all annotations in the set.
        """
        return np.array([a.text for a in self.annotations])

    def to_dataframe(self):
        """
        Return a DataFrame with the annotations.

        Returns:
            (pd.DataFrame): DataFrame object with annotations.
        """
        onsets = []
        durations = []
        texts = []
        for annot in self:
            onsets.append(annot.onset)
            durations.append(annot.duration)
            texts.append(annot.text)
        return pd.DataFrame({'onset': onsets, 'duration': durations, 'text': texts})

    def to_df(self):
        return self.to_dataframe()