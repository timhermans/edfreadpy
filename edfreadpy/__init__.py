"""
Package for reading (and anonymizing) EDF and EDF+ files.
"""
from edfreadpy.annotations.annotation import Annotation
from edfreadpy.annotations.annotation_set import AnnotationSet
from edfreadpy.io.reader import EdfReader

__all__ = [
    'Annotation',
    'AnnotationSet',
    'EdfReader',
]
